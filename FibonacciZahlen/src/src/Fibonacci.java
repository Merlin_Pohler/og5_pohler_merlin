package src;

/* Fibonacci.java
Programm zum Testen der rekursiven und der iterativen Funktion
zum Berechnen der Fibonacci-Zahlen.
AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
Autor:Merlin
Version: 1.0
Datum:
*/
public class Fibonacci{
// Konstruktor
Fibonacci(){
}

/**
* Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
* @param n 
* @return die n-te Fibonacci-Zahl
 arbeitet in mehreren Ebenen
*/
long fiboRekursiv(int n){
 if (n < 2) {
    return n;
   }
   return fiboRekursiv(n - 2) + fiboRekursiv(n - 1); 
}//fiboRekursiv

/**
* Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
* @param n 
* @return die n-te Fibonacci-Zahl
 arbeitet in einer Ebene
*/
long fiboIterativ(int n){
   if (n < 2) {
    return n;
   }
   long x = 0;
   long a=0 , b=1;
   for (int i = 1; i < n; i++) {
     x=a+b;
     a=b;
     b=x;
   }
   return x;
}//fiboIterativ

}// Fibonnaci
