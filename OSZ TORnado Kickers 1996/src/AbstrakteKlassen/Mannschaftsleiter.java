package AbstrakteKlassen;

public class Mannschaftsleiter extends Spieler {

	private String mannschaftsname;
	private String engagement;
	
	
	public Mannschaftsleiter() {
	}
	
	public Mannschaftsleiter(String name, int telefonnummer,int trikotnummer, String spielposition, double jahresbeitrag,String mannschaftsname, String engagement){
	
	super(name,telefonnummer,trikotnummer,spielposition,jahresbeitrag);	
	this.mannschaftsname = mannschaftsname;
	this.engagement = engagement;
	
	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public String getEngagement() {
		return engagement;
	}

	public void setEngagement(String engagement) {
		this.engagement = engagement;
	}
	
	public void organisieren() {
		
		
		
	}
	
}
