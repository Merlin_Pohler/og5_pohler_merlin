package AbstrakteKlassen;

public class Schiedsrichter extends Person {

	private int gepfiffeneSpiele;
	
	public Schiedsrichter() {
	}
	
	public Schiedsrichter(String name, int telefonnummer,int gepfiffeneSpiele){
	
	super(name,telefonnummer);	
	this.gepfiffeneSpiele = gepfiffeneSpiele;
	
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
	
	public void pfeiffen(){
		
		
	}
}
