package AbstrakteKlassen;

public class Spieler extends Person {

	private int trikotnummer;
	private String spielposition;
	private double jahresbeitrag;
	
	public Spieler() {
	}
	
	public Spieler(String name, int telefonnummer,int trikotnummer, String spielposition, double jahresbeitrag){
	
	super(name , telefonnummer);	
	this.trikotnummer = trikotnummer;
	this.spielposition = spielposition;
	this.jahresbeitrag = jahresbeitrag;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public double getJahresbeitrag() {
		return jahresbeitrag;
	}

	public void setJahresbeitrag(double jahresbeitrag) {
		this.jahresbeitrag = jahresbeitrag;
	}
	
	public void spielen() {
		
		
		
	}
	
	
	
	
}
