
public class MiniMath {
	
	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * @param n - Angabe der Zahl
	 * @return n!
	 */
	public static int berechneFakultaet(int n){
		
		if(n==0){
			return 1;
			
		}else{
			return berechneFakultaet(n-1)*n;
		}
		
		
	}
	
	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * @param n - Angabe der Potenz (max. 31 sonst Integeroverflow)
	 * @return 2^n 
	 */
	public static int berechneZweiHoch(int n){
		
		n = 2^n;
		
		if(n<31){
			
			return n;
			
		}else{
			
			System.out.print("Sry weiter gehts nich");
			
		}
		
		
		return 0;
	}
	
	/**
	 * Die Methode berechnet die Summe der Zahlen von 
	 * 1 bis n (also 1+2+...+(n-1)+n)
	 * @param n - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static int berechneSumme(int n){
		return 0;
	}
	
}

