package addon;

public class Addon {

	
//attribute
	private int idNummer;
	private String bezeichnung;
	private double preis;
	private int bestand;
	private int maxBestand;
	
	
//Constructor	
	public Addon(){
		
		
	}
	
	
	
	//getter & setter	
	public int getIdNummer() {
		return idNummer;
	}
	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public double getPreis() {
		return preis;
	}
	public void setPreis(double preis) {
		this.preis = preis;
	}
	public int getBestand() {
		return bestand;
	}
	public void setBestand(int bestand) {
		this.bestand = bestand;
	}
	public int getMaxBestand() {
		return maxBestand;
	}
	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
	
	public void hinzufuegen(int hin){
		this.bestand = bestand + hin; 
		}
	public void reduzieren(int red){
		this.bestand = bestand + red;
	}
	public double gesamtwert(double ges){
		return ges = preis * bestand;
	}
	
}
