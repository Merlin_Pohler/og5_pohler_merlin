package filecontrol;

import java.util.List;

import filecontrol.FileControl;
import filecontrol.LearnerData;
import filecontrol.Lehreintrag;

import java.util.List;

public class Logic {

	private List<Lehreintrag> listEntries;
	private String learnerName;
	private final FileControl FILE_CONTROL;

	public Logic(FileControl fc) {
		this.FILE_CONTROL = fc;
		reloadFromDisk();
	}

	public void reloadFromDisk() {
		LearnerData ldata = FILE_CONTROL.getFileContent();
		this.learnerName = ldata.getLearnerName();
		this.listEntries = ldata.getListEntries();
	}

	public List<Lehreintrag> getListEntries() {
		return listEntries;
	}

	public void setListEntries(List<Lehreintrag> listEntries) {
		this.listEntries = listEntries;
	}

	public String getLearnerName() {
		return learnerName;
	}

	public boolean addEntry(Lehreintrag l) {
		if (FILE_CONTROL.addEntry(l)) {
			listEntries.add(l);
			return true;
		}
		return false;
	}

	public String createReport() {
		
		return "";
	
	}

}


