package filecontrol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

public class FileControl {

	private String filename;
	private Lehreintrag lehreintrag;

	public FileControl(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public LearnerData getFileContent() {
		ArrayList<Lehreintrag> list = new ArrayList<Lehreintrag>();
		LearnerData ldata = new LearnerData("Unbekannt", list);

		// liest einen Datenstrom aus einer Datei
		FileReader fr;
		// bereitet den Datenstrom auf
		BufferedReader br;
		// versuch eine Operation durchzuführen
		try {
			File f = new File(filename);
			System.out.format("Lese Datei %s ein...%n", f.getAbsoluteFile());
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			String s = br.readLine();
			ldata.setLearnerName(s);
			s = br.readLine();
			while (s != null) {
				Date datum = SofaLearningHelper.stringToDate(s);
				String fach = br.readLine();
				String eintrag = br.readLine();
				int dauer = Integer.parseInt(br.readLine());
				list.add(new Lehreintrag(datum, fach, eintrag, dauer));
				s = br.readLine();
			}

			// Reader schließen um keinen Ressourcenkonflikt zu verursachen
			br.close();
			// Fehlerfälle abfangen
		} catch (Exception e) {
			System.err.println("Fehler beim Einlesen der Datei " + filename);
			e.printStackTrace();
		}
		return ldata;
	}

	public boolean addEntry(Lehreintrag eintrag) {
		PrintWriter pWriter = null;
		try {
			pWriter = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
			pWriter.println(SofaLearningHelper.dateToString(eintrag.getDatum()));
			pWriter.println(eintrag.getFach());
			pWriter.println(eintrag.getBeschreibung());
			pWriter.println(eintrag.getDauer());
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		} finally {
			if (pWriter != null) {
				pWriter.flush();
				pWriter.close();
			}
		}
		return true;
	}

}
