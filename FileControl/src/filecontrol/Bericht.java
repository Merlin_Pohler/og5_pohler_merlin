package filecontrol;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

public class Bericht extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton btnBerichtSch;
	private JButton btnBerichtSpeichern;
	private  MainWindow ley;
	/**
	 * 
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bericht frame = new Bericht(new MainWindow());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bericht(MainWindow _ley) {
		this.ley=_ley;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblBerichtEintragen = new JLabel("Bericht eintragen");
		panel.add(lblBerichtEintragen);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		 btnBerichtSpeichern = new JButton("Bericht speichern");
		panel_2.add(btnBerichtSpeichern);
		
		btnBerichtSch = new JButton("Bericht schlie\u00DFen");
		panel_2.add(btnBerichtSch);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		btnBerichtSpeichern.addActionListener(this);
		btnBerichtSch.addActionListener(this);
		this.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnBerichtSch)
		{
			this.dispose();
		}
		if (e.getSource() == btnBerichtSpeichern)
		{
			
		}
		
		
	}
}
