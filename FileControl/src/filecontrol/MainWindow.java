package filecontrol;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

public class MainWindow extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton btnNeuerEintrag;
	private JButton btnBerichte;
	private JButton btnBeenden;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		 btnNeuerEintrag = new JButton("neuer Eintrag");
		 btnNeuerEintrag.addActionListener(this);
		panel_1.add(btnNeuerEintrag);
		
		 btnBerichte = new JButton("Berichte");
		 btnBerichte.addActionListener(this);
		panel_1.add(btnBerichte);
		
		 btnBeenden = new JButton("Beenden");
		 btnBeenden.addActionListener(this);
		panel_1.add(btnBeenden);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.NORTH);
		
		JLabel lblLerneintr = new JLabel("Lerntagebuch von Merlin");
		lblLerneintr.setHorizontalAlignment(SwingConstants.LEFT);
		panel_2.add(lblLerneintr);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		JTextPane textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnNeuerEintrag)
		{
			new NeuerEintrag(this);
		}
		
		if (e.getSource() == btnBerichte)
		{
			
			new Bericht(this);
		}
		
		if (e.getSource() == btnBeenden)
		{
			
			System.exit(1);
			
		}
	}

}
