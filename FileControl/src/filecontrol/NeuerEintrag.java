package filecontrol;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class NeuerEintrag extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField txtFach;
	private JTextField txtBeschr;
	private JTextField txtDatum;
	private MainWindow lay;
	private JButton btnFertig;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NeuerEintrag frame = new NeuerEintrag(new MainWindow());

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NeuerEintrag(MainWindow _lay) {
		this.lay = _lay;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(5, 2, 0, 8));

		JLabel lblNeuerEintrag = new JLabel("Neuer Eintrag");
		lblNeuerEintrag.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 27));
		contentPane.add(lblNeuerEintrag);

		JLabel label = new JLabel("");
		contentPane.add(label);

		JLabel lblFach = new JLabel("Fach");
		lblFach.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblFach);

		txtFach = new JTextField();
		contentPane.add(txtFach);
		txtFach.setColumns(10);

		JLabel lblBeschr = new JLabel("Beschreibung");
		lblBeschr.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblBeschr);

		txtBeschr = new JTextField();
		contentPane.add(txtBeschr);
		txtBeschr.setColumns(10);

		JLabel lblDatum = new JLabel("Datum");
		lblDatum.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblDatum);

		txtDatum = new JTextField();
		contentPane.add(txtDatum);
		txtDatum.setColumns(10);

		JLabel lblNewLabel = new JLabel("");
		contentPane.add(lblNewLabel);

		JButton btnFertig = new JButton("Fertig");
		contentPane.add(btnFertig);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (e.getSource() == btnFertig)
		{
	
	
	}

	}
}
