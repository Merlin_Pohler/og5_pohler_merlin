package filecontrol;

import java.util.List;

public class LearnerData {
	private String learnerName;
	private List<Lehreintrag> listEntries;

	public LearnerData(String name, List<Lehreintrag> list) {
		this.setLearnerName(name);
		this.setListEntries(list);
	}

	public String getLearnerName() {
		return learnerName;
	}

	public void setLearnerName(String learnerName) {
		this.learnerName = learnerName;
	}

	public List<Lehreintrag> getListEntries() {
		return listEntries;
	}

	public void setListEntries(List<Lehreintrag> listEntries) {
		this.listEntries = listEntries;
	}

}
