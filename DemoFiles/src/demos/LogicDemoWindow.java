package demos;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class LogicDemoWindow extends JFrame implements ActionListener {

	private JButton btnClick;
	private JLabel lblHallo;

	private Logic meineLogik;
	
	public LogicDemoWindow(Logic _meineLogik) {
		this.meineLogik = _meineLogik;
		
		JPanel meinPanel = new JPanel();
		meinPanel.setLayout(new BorderLayout());
		JPanel meineButtons = new JPanel();
		meineButtons.setLayout(new FlowLayout());
		
		JTextArea tarText = new JTextArea();
		// Consolas, courier, Courier New
		tarText.setFont(new Font("Consolas", Font.PLAIN, 16));
		tarText.append(meineLogik.getListAsString());
		
		JScrollPane pnlScrollMe = new JScrollPane(tarText);
		pnlScrollMe.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
				
		lblHallo = new JLabel("Guten Morgen!");
		btnClick = new JButton("Klickste mich ma bitte!");
		meinPanel.add(lblHallo, BorderLayout.NORTH);
		meineButtons.add(btnClick);
		meinPanel.add(meineButtons, BorderLayout.SOUTH);
		meinPanel.add(pnlScrollMe, BorderLayout.CENTER);
		
		btnClick.addActionListener(this);

		this.setContentPane(meinPanel);

		this.pack();
		this.setVisible(true);
	}
	
	public void empfangeNachricht(String hi)
	{
		this.lblHallo.setText(hi);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnClick) {
			// Hier soll was passieren
			this.dispose();
		}
	}

	public static void main(String[] args) {
		new LogicDemoWindow(new Logic());
	}

}
