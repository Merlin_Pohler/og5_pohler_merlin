package demos;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainWindow extends JFrame implements ActionListener {

	private JButton btnClick;
	private JLabel lblHallo;

	public MainWindow() {
		JPanel meinPanel = new JPanel();
		meinPanel.setLayout(new FlowLayout());

		btnClick = new JButton("Klick mich!");
		lblHallo = new JLabel("Guten Morgen!");

		meinPanel.add(lblHallo);
		meinPanel.add(btnClick);

		btnClick.addActionListener(this);

		this.setContentPane(meinPanel);

		this.pack();
		this.setVisible(true);
	}
	
	public void empfangeNachricht(String hi)
	{
		this.lblHallo.setText(hi);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnClick) {
			// Hier soll was passieren
				new SubWindow(this);
		}
	}

	public static void main(String[] args) {
		new MainWindow();
	}

}
