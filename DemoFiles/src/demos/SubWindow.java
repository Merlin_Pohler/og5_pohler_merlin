package demos;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SubWindow extends JFrame implements ActionListener {

	private JButton btnClick;
	private JButton btnSend;
	private int counter = 0;
	
	private MainWindow meinHauptfenster;
	
	public SubWindow(MainWindow m) {
		super();
		this.meinHauptfenster = m;		
		JPanel meinPanel = new JPanel();
		meinPanel.setLayout(new FlowLayout());
		
		btnClick = new JButton("Schlie� mich!");
		btnSend =  new JButton("Sende was!");
		JLabel  lblHallo = new JLabel("Guten Morgen!");
		
		meinPanel.add(lblHallo);
		meinPanel.add(btnClick);
		meinPanel.add(btnSend);
		btnClick.addActionListener(this);
		btnSend.addActionListener(this);
		
		this.setContentPane(meinPanel);

		this.pack();
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnClick)
		{
			this.dispose();
		}
		if (e.getSource() == btnSend)
		{
			meinHauptfenster.empfangeNachricht("Hier ist Albraa Nr" + counter);
			counter++;
			
		}
	}

}