package demos;

import java.util.ArrayList;
import java.util.List;

public class Logic {
	private ArrayList<String> meineListe;

	public Logic() {
		meineListe = new ArrayList<String>();

		String s = "s";
		for (int beers = 99; beers > -1;) {
			meineListe.add(beers + " bottle" + s + " of milk on the wall, ");
			meineListe.add(beers + " bottle" + s + " of milk, ");
			if (beers == 0) {
				meineListe.add("Go to the store, buy some more, ");
				meineListe.add("99 bottles of milk on the wall.\n");
				break;
			} else
				meineListe.add("Take one down, pass it around, ");
			s = (--beers == 1) ? "" : "s";
			meineListe.add(beers + " bottle" + s + " of milk on the wall.\n");
		}
	}

	public String getListAsString() {
		String s = "";
		for (String line : meineListe) {
			s = s + line;
		}
		return s;
	}
	
	public List<String> getList()
	{
		return this.meineListe;
	}

}
